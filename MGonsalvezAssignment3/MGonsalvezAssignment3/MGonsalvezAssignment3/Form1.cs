﻿//Mel Joe Antony Gonsalvez
// Assignment 3

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MGonsalvezAssignment3
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Variables declared globally
        /// </summary>
        Boolean player1Check = true;
        string playerOne = "Player 1";
        string playerTwo = "Player 2";
        int tieCheck = 0;
        Image imageX = Properties.Resources.X;
        Image imageO = Properties.Resources._0;

        public Form1()
        {
            InitializeComponent();

            playerStatus.Text = playerOne;
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// PlayerTurn method for changing player status and to call checkWinner function and to check the game is tie or not.
        /// </summary>
        public void PlayerTurn()
        {

            if(player1Check == true)
            {
                player1Check = false;
                playerStatus.Text = playerTwo;
            }
            else
            {
                player1Check = true;
                playerStatus.Text = playerOne;
            }
            tieCheck++;
            CheckWinner();



        }

        /// <summary>
        /// Using single event handler for all the PictureBoxes for adding image and to call PlayerTurn 
        /// method after each click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox_Click(object sender, EventArgs e)
        {
            PictureBox selectedPicBox = sender as PictureBox;
            selectedPicBox.Image = player1Check ? imageX : imageO;
            PlayerTurn();
            selectedPicBox.Enabled = false;
        }

        /// <summary>
        /// CheckWinner method checks whether images are same in rows or coloumns or diagonally
        /// it is done by adding the pictureBoxes into an 2D Array.
        /// </summary>
        public void CheckWinner()
        {
            PictureBox[,] pictureBoxes = {
                {gameBox1, gameBox2, gameBox3 },
                {gameBox4, pictureBox5, gameBox6 },
                {pictureBox7, gameBox8, gameBox9 }

            };

            for (int row = 0; row < 3; row++)
            {
                if (pictureBoxes[row, 0].Image != null && pictureBoxes[row, 0].Image == pictureBoxes[row, 1].Image && pictureBoxes[row, 1].Image == pictureBoxes[row, 2].Image)
                {
                    
                    if(pictureBoxes[row, 0].Image == imageX)
                    {
                        Winner(playerOne);
                    }
                    else
                    {
                        Winner(playerTwo);
                    }
                }
                else if (pictureBoxes[0, row].Image != null && pictureBoxes[0, row].Image == pictureBoxes[1, row].Image && pictureBoxes[1, row].Image == pictureBoxes[2, row].Image)
                {
                    if (pictureBoxes[0, row].Image == imageX)
                    {
                        Winner(playerOne);
                    }
                    else
                    {
                        Winner(playerTwo);
                    }
                }
            }

            if (pictureBoxes[0, 0].Image != null && pictureBoxes[0, 0].Image == pictureBoxes[1, 1].Image && pictureBoxes[1, 1].Image == pictureBoxes[2, 2].Image)
            {
                if (pictureBoxes[0, 0].Image == imageX)
                {
                    Winner(playerOne);
                }
                else
                {
                    Winner(playerTwo);
                }
            }
            else if(pictureBoxes[0, 2].Image != null && pictureBoxes[0, 2].Image == pictureBoxes[1, 1].Image && pictureBoxes[1, 1].Image == pictureBoxes[2, 0].Image)
            {
                if (pictureBoxes[0, 2].Image == imageX)
                {
                    Winner(playerOne);
                }
                else
                {
                    Winner(playerTwo);
                }
            }

            if(tieCheck == 9)
            {
                MessageBox.Show("Game is Tied");
                ResetGame();
            }



        }

        /// <summary>
        /// Winner method takes player which won as paramenter and displays it and  calls Reset
        ///  method.
        /// </summary>
        /// <param name="player"></param>
        public void Winner(string player)
        {

            playerStatus.Text = player + " Won!!";
            
            //foreach (Control picBox in Controls)
            //{
            //    PictureBox box = picBox as PictureBox;
            //    box.Enabled = true;
            //    box.Image = null;
            //}

            
            player1Check = true;

            MessageBox.Show(player + " Won");
            ResetGame();

        }


        /// <summary>
        /// ResetGame method is called when a player wins or the game goes tie, to reset the game.
        /// </summary>
        public void ResetGame()
        {
            tieCheck = 0;
            player1Check = true;
            playerStatus.Text = playerOne;
            PictureBox[,] pictureBoxes = {
                {gameBox1, gameBox2, gameBox3 },
                {gameBox4, pictureBox5, gameBox6 },
                {pictureBox7, gameBox8, gameBox9 }

            };

            for (int i = 0; i < 3; i++)
            {

                for (int j = 0; j < 3; j++)
                {
                    pictureBoxes[i, j].Enabled = true;
                    pictureBoxes[i, j].Image = null;
                    
                    

                }
            }
        }

    }
}
