﻿namespace MGonsalvezAssignment3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gameBox1 = new System.Windows.Forms.PictureBox();
            this.gameBox2 = new System.Windows.Forms.PictureBox();
            this.gameBox3 = new System.Windows.Forms.PictureBox();
            this.gameBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.gameBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.gameBox8 = new System.Windows.Forms.PictureBox();
            this.gameBox9 = new System.Windows.Forms.PictureBox();
            this.playerStatus = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox9)).BeginInit();
            this.SuspendLayout();
            // 
            // gameBox1
            // 
            this.gameBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gameBox1.Location = new System.Drawing.Point(211, 88);
            this.gameBox1.Name = "gameBox1";
            this.gameBox1.Size = new System.Drawing.Size(107, 89);
            this.gameBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.gameBox1.TabIndex = 0;
            this.gameBox1.TabStop = false;
            this.gameBox1.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // gameBox2
            // 
            this.gameBox2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gameBox2.Location = new System.Drawing.Point(341, 88);
            this.gameBox2.Name = "gameBox2";
            this.gameBox2.Size = new System.Drawing.Size(107, 89);
            this.gameBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.gameBox2.TabIndex = 1;
            this.gameBox2.TabStop = false;
            this.gameBox2.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // gameBox3
            // 
            this.gameBox3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gameBox3.Location = new System.Drawing.Point(474, 88);
            this.gameBox3.Name = "gameBox3";
            this.gameBox3.Size = new System.Drawing.Size(107, 89);
            this.gameBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.gameBox3.TabIndex = 2;
            this.gameBox3.TabStop = false;
            this.gameBox3.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // gameBox4
            // 
            this.gameBox4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gameBox4.Location = new System.Drawing.Point(211, 199);
            this.gameBox4.Name = "gameBox4";
            this.gameBox4.Size = new System.Drawing.Size(107, 89);
            this.gameBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.gameBox4.TabIndex = 3;
            this.gameBox4.TabStop = false;
            this.gameBox4.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox5.Location = new System.Drawing.Point(341, 199);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(107, 89);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // gameBox6
            // 
            this.gameBox6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gameBox6.Location = new System.Drawing.Point(474, 199);
            this.gameBox6.Name = "gameBox6";
            this.gameBox6.Size = new System.Drawing.Size(107, 89);
            this.gameBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.gameBox6.TabIndex = 5;
            this.gameBox6.TabStop = false;
            this.gameBox6.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox7.Location = new System.Drawing.Point(211, 317);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(107, 89);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 6;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // gameBox8
            // 
            this.gameBox8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gameBox8.Location = new System.Drawing.Point(341, 317);
            this.gameBox8.Name = "gameBox8";
            this.gameBox8.Size = new System.Drawing.Size(107, 89);
            this.gameBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.gameBox8.TabIndex = 7;
            this.gameBox8.TabStop = false;
            this.gameBox8.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // gameBox9
            // 
            this.gameBox9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.gameBox9.Location = new System.Drawing.Point(474, 317);
            this.gameBox9.Name = "gameBox9";
            this.gameBox9.Size = new System.Drawing.Size(107, 89);
            this.gameBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.gameBox9.TabIndex = 8;
            this.gameBox9.TabStop = false;
            this.gameBox9.Click += new System.EventHandler(this.pictureBox_Click);
            // 
            // playerStatus
            // 
            this.playerStatus.AutoSize = true;
            this.playerStatus.Location = new System.Drawing.Point(377, 25);
            this.playerStatus.Name = "playerStatus";
            this.playerStatus.Size = new System.Drawing.Size(46, 17);
            this.playerStatus.TabIndex = 9;
            this.playerStatus.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.playerStatus);
            this.Controls.Add(this.gameBox9);
            this.Controls.Add(this.gameBox8);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.gameBox6);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.gameBox4);
            this.Controls.Add(this.gameBox3);
            this.Controls.Add(this.gameBox2);
            this.Controls.Add(this.gameBox1);
            this.Name = "Form1";
            this.Text = "TIC TAC TOE";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gameBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gameBox9)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label playerStatus;
        private System.Windows.Forms.PictureBox gameBox1;
        private System.Windows.Forms.PictureBox gameBox2;
        private System.Windows.Forms.PictureBox gameBox3;
        private System.Windows.Forms.PictureBox gameBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox gameBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox gameBox8;
        private System.Windows.Forms.PictureBox gameBox9;
    }
}

