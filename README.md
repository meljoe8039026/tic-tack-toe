

## How to play

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Clone the file from repository 
2. Unzip the tic-tack-toe folder
3. Open the unzipped folder
4. Click on the .sln flie
5. After opening the file in Visual Studio click on Build.
6. Enjoy the Game.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## About License

I used this License because it is from a free open source project as mine and it mentions the redistribution copy should retain the same 
copyright and license.

I copied  this license from github of OpenCV https://github.com/opencv/opencv/blob/master/LICENSE



